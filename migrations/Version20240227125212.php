<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240227125212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Data structure for the seminars (V1)';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE organisation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, acronym VARCHAR(255) DEFAULT NULL, homepage VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE organisation_translation (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, organisation_id INT NOT NULL, INDEX IDX_7C36F9F99E6B1585 (organisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, given_name VARCHAR(255) DEFAULT NULL, family_name VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, homepage VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE presentation (id INT AUTO_INCREMENT NOT NULL, original_title VARCHAR(255) NOT NULL, beginning DATETIME NOT NULL, ending DATETIME DEFAULT NULL, followed_by_me TINYINT(1) NOT NULL, language VARCHAR(255) NOT NULL, seminar_id INT NOT NULL, INDEX IDX_9B66E893735A6AB8 (seminar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar_speaker (presentation_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_BC956299AB627E8B (presentation_id), INDEX IDX_BC956299217BBB47 (person_id), PRIMARY KEY(presentation_id, person_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar_respondant (presentation_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_D4F5DEB6AB627E8B (presentation_id), INDEX IDX_D4F5DEB6217BBB47 (person_id), PRIMARY KEY(presentation_id, person_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE presentation_translation (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(255) NOT NULL, abstract LONGTEXT DEFAULT NULL, takeouts LONGTEXT DEFAULT NULL, notes LONGTEXT DEFAULT NULL, presentation_id INT NOT NULL, INDEX IDX_F85A4D4BAB627E8B (presentation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, acronym VARCHAR(255) DEFAULT NULL, homepage VARCHAR(255) DEFAULT NULL, followed_by_me TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar_organisation (seminar_id INT NOT NULL, organisation_id INT NOT NULL, INDEX IDX_6642CAC1735A6AB8 (seminar_id), INDEX IDX_6642CAC19E6B1585 (organisation_id), PRIMARY KEY(seminar_id, organisation_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar_person (seminar_id INT NOT NULL, person_id INT NOT NULL, INDEX IDX_F3CD9D0D735A6AB8 (seminar_id), INDEX IDX_F3CD9D0D217BBB47 (person_id), PRIMARY KEY(seminar_id, person_id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('CREATE TABLE seminar_translation (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, seminar_id INT NOT NULL, INDEX IDX_72F6FBFA735A6AB8 (seminar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4');
        $this->addSql('ALTER TABLE organisation_translation ADD CONSTRAINT FK_7C36F9F99E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id)');
        $this->addSql('ALTER TABLE presentation ADD CONSTRAINT FK_9B66E893735A6AB8 FOREIGN KEY (seminar_id) REFERENCES seminar (id)');
        $this->addSql('ALTER TABLE seminar_speaker ADD CONSTRAINT FK_BC956299AB627E8B FOREIGN KEY (presentation_id) REFERENCES presentation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_speaker ADD CONSTRAINT FK_BC956299217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_respondant ADD CONSTRAINT FK_D4F5DEB6AB627E8B FOREIGN KEY (presentation_id) REFERENCES presentation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_respondant ADD CONSTRAINT FK_D4F5DEB6217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE presentation_translation ADD CONSTRAINT FK_F85A4D4BAB627E8B FOREIGN KEY (presentation_id) REFERENCES presentation (id)');
        $this->addSql('ALTER TABLE seminar_organisation ADD CONSTRAINT FK_6642CAC1735A6AB8 FOREIGN KEY (seminar_id) REFERENCES seminar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_organisation ADD CONSTRAINT FK_6642CAC19E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_person ADD CONSTRAINT FK_F3CD9D0D735A6AB8 FOREIGN KEY (seminar_id) REFERENCES seminar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_person ADD CONSTRAINT FK_F3CD9D0D217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seminar_translation ADD CONSTRAINT FK_72F6FBFA735A6AB8 FOREIGN KEY (seminar_id) REFERENCES seminar (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE organisation_translation DROP FOREIGN KEY FK_7C36F9F99E6B1585');
        $this->addSql('ALTER TABLE presentation DROP FOREIGN KEY FK_9B66E893735A6AB8');
        $this->addSql('ALTER TABLE seminar_speaker DROP FOREIGN KEY FK_BC956299AB627E8B');
        $this->addSql('ALTER TABLE seminar_speaker DROP FOREIGN KEY FK_BC956299217BBB47');
        $this->addSql('ALTER TABLE seminar_respondant DROP FOREIGN KEY FK_D4F5DEB6AB627E8B');
        $this->addSql('ALTER TABLE seminar_respondant DROP FOREIGN KEY FK_D4F5DEB6217BBB47');
        $this->addSql('ALTER TABLE presentation_translation DROP FOREIGN KEY FK_F85A4D4BAB627E8B');
        $this->addSql('ALTER TABLE seminar_organisation DROP FOREIGN KEY FK_6642CAC1735A6AB8');
        $this->addSql('ALTER TABLE seminar_organisation DROP FOREIGN KEY FK_6642CAC19E6B1585');
        $this->addSql('ALTER TABLE seminar_person DROP FOREIGN KEY FK_F3CD9D0D735A6AB8');
        $this->addSql('ALTER TABLE seminar_person DROP FOREIGN KEY FK_F3CD9D0D217BBB47');
        $this->addSql('ALTER TABLE seminar_translation DROP FOREIGN KEY FK_72F6FBFA735A6AB8');
        $this->addSql('DROP TABLE organisation');
        $this->addSql('DROP TABLE organisation_translation');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE presentation');
        $this->addSql('DROP TABLE seminar_speaker');
        $this->addSql('DROP TABLE seminar_respondant');
        $this->addSql('DROP TABLE presentation_translation');
        $this->addSql('DROP TABLE seminar');
        $this->addSql('DROP TABLE seminar_organisation');
        $this->addSql('DROP TABLE seminar_person');
        $this->addSql('DROP TABLE seminar_translation');
    }
}
