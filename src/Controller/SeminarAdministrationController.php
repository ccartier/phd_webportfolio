<?php

namespace App\Controller;

use App\Entity\Seminar;
use App\Entity\SeminarTranslation;
use App\Entity\Presentation;
use App\Entity\PresentationTranslation;
use App\Entity\Organisation;
use App\Entity\OrganisationTranslation;
use App\Form\SeminarType;
use App\Form\PresentationType;
use App\Form\OrganisationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class SeminarAdministrationController extends AbstractController
{
    #[Route('/edit/seminar', name: 'app_seminar_edit')]
    public function edit_seminar(Request $request, EntityManagerInterface $entityManager): Response
    {
        $seminar = new Seminar();

        $frenchTranslation = new SeminarTranslation();
        $frenchTranslation->setLocale('fr');

        $englishTranslation = new SeminarTranslation();
        $englishTranslation->setLocale('en');

        $seminar->addTranslation($frenchTranslation)->addTranslation($englishTranslation);

        $form = $this->createForm(SeminarType::class, $seminar);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($seminar);
            $entityManager->persist($frenchTranslation);
            $entityManager->persist($englishTranslation);
            $entityManager->flush();
            return $this->redirectToRoute('app_seminar_presentation_edit');
        }

        return $this->render('seminar_administration/index.html.twig', [
            'title' => 'Edit seminar',
            'form' => $form,
        ]);
    }

    #[Route('/edit/presentation', name: 'app_seminar_presentation_edit')]
    public function edit_presentation(Request $request, EntityManagerInterface $entityManager): Response
    {
        $presentation = new Presentation();

        $frenchTranslation = new PresentationTranslation();
        $frenchTranslation->setLocale('fr');

        $englishTranslation = new PresentationTranslation();
        $englishTranslation->setLocale('en');

        $presentation->addTranslation($frenchTranslation)->addTranslation($englishTranslation);

        $form = $this->createForm(PresentationType::class, $presentation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($presentation);
            $entityManager->persist($frenchTranslation);
            $entityManager->persist($englishTranslation);
            $entityManager->flush();
        }

        return $this->render('seminar_administration/index.html.twig', [
            'title' => 'Edit presentation',
            'form' => $form,
        ]);
    }

    #[Route('/edit/organisation', name: 'app_organisation_edit')]
    public function edit_organisation(Request $request, EntityManagerInterface $entityManager): Response
    {
        $organisation = new Organisation();

        $frenchTranslation = new OrganisationTranslation();
        $frenchTranslation->setLocale('fr');

        $englishTranslation = new OrganisationTranslation();
        $englishTranslation->setLocale('en');

        $organisation->addTranslation($frenchTranslation)->addTranslation($englishTranslation);

        $form = $this->createForm(OrganisationType::class, $organisation);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($organisation);
            $entityManager->persist($frenchTranslation);
            $entityManager->persist($englishTranslation);
            $entityManager->flush();
            return $this->redirectToRoute('app_seminar_edit');
        }

        return $this->render('seminar_administration/index.html.twig', [
            'title' => 'Edit organisation',
            'form' => $form,
        ]);
    }
}
