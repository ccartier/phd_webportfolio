<?php

namespace App\Controller;

use App\Entity\Seminar;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class PortfolioController extends AbstractController
{
    #[Route(path: ['fr' => '/', 'en' => '/{_locale}'], name: 'app_index')]
    public function landing(): Response
    {
        return $this->render('index.html.twig');
    }

    #[Route(path: [
        'fr' => '/portfolio',
        'en' => '/{_locale}/portfolio',
    ], name: 'app_portfolio')]
    public function index(): Response
    {
        return $this->render('portfolio/index.html.twig');
    }

    #[Route(path: [
        'fr' => '/portfolio/formation',
        'en' => '/{_locale}/portfolio/training',
    ], name: 'app_portfolio_training')]
    public function displayTraining(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Seminar::class);
        $seminars = $repository->findBy(['followedByMe' => true]);
        return $this->render('portfolio/training.html.twig', [
            'seminars' => $seminars,
        ]);
    }

    #[Route(path: [
        'fr' => '/portfolio/recherche',
        'en' => '/{_locale}/portfolio/research',
    ], name: 'app_portfolio_research')]
    public function displayResearch(EntityManagerInterface $entityManager): Response
    {
        $me = $this->getOrCreateMe($entityManager);
        return $this->render('portfolio/research.html.twig', [
            'seminars' => $me->getOrganisedSeminars(),
            'presentations' => $me->getPresentations(),
        ]);
    }

    protected function getOrCreateMe(EntityManagerInterface $entityManager): Person
    {
        $repository = $entityManager->getRepository(Person::class);
        $me = $repository->findOneBy(['homepage' => 'https://catgolin.eu']);
        if ($me) {
            return $me;
        }
        $me = new Person();
        $me->setGivenName('Clément');
        $me->setFamilyName('Cartier');
        $me->setHomepage('https://catgolin.eu');
        $entityManager->persist($me);
        $entityManager->flush();
        return $me;
    }

    #[Route(path: ['fr' => '/portfolio/mediation-scientifique',
                   'en' => '/{_locale}/portfolio/popularisation',
    ], name: 'app_portfolio_popularisation')]
    public function displayPopularisation(): Response
    {
        return $this->render('portfolio/popularisation.html.twig');
    }

    #[Route(path: ['fr' => '/portfolio/responsabilites-collectives',
                   'en' => '/{_locale}/portfolio/collective-responsabilities',
    ], name: 'app_portfolio_responsabilities')]
    public function displayResponsabilities(): Response
    {
        return $this->render('portfolio/responsabilities.html.twig');
    }

    #[Route(path: ['fr' => '/portfolio/activites-diverses',
                   'en' => '/{_locale}/portfolio/miscellaneous',
    ], name: 'app_portfolio_miscellaneous')]
    public function displayMiscellaneous(): Response
    {
        return $this->render('portfolio/miscelanneous.html.twig');
    }
}
