<?php

namespace App\Repository;

use App\Entity\SeminarTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SeminarTranslation>
 *
 * @method SeminarTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeminarTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeminarTranslation[]    findAll()
 * @method SeminarTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeminarTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeminarTranslation::class);
    }

    //    /**
    //     * @return SeminarTranslation[] Returns an array of SeminarTranslation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?SeminarTranslation
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
