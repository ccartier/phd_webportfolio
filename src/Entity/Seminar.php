<?php

namespace App\Entity;

use App\Repository\SeminarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SeminarRepository::class)]
class Seminar
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $acronym = null;

    #[ORM\ManyToMany(targetEntity: Organisation::class, inversedBy: 'seminars')]
    private Collection $organisation;

    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: 'organisedSeminars')]
    private Collection $organisers;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $homepage = null;

    #[ORM\Column]
    private ?bool $followedByMe = null;

    #[ORM\OneToMany(targetEntity: SeminarTranslation::class, mappedBy: 'seminar', orphanRemoval: true)]
    private Collection $translations;

    #[ORM\OneToMany(targetEntity: Presentation::class, mappedBy: 'seminar', orphanRemoval: true)]
    private Collection $presentations;

    public function __construct()
    {
        $this->organisation = new ArrayCollection();
        $this->organisers = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->presentations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(?string $acronym): static
    {
        $this->acronym = $acronym;

        return $this;
    }

    /**
     * @return Collection<int, Organisation>
     */
    public function getOrganisation(): Collection
    {
        return $this->organisation;
    }

    public function addOrganisation(Organisation $organisation): static
    {
        if (!$this->organisation->contains($organisation)) {
            $this->organisation->add($organisation);
        }

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): static
    {
        $this->organisation->removeElement($organisation);

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getOrganisers(): Collection
    {
        return $this->organisers;
    }

    public function addOrganiser(Person $organiser): static
    {
        if (!$this->organisers->contains($organiser)) {
            $this->organisers->add($organiser);
        }

        return $this;
    }

    public function removeOrganiser(Person $organiser): static
    {
        $this->organisers->removeElement($organiser);

        return $this;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function setHomepage(?string $homepage): static
    {
        $this->homepage = $homepage;

        return $this;
    }

    public function isFollowedByMe(): ?bool
    {
        return $this->followedByMe;
    }

    public function setFollowedByMe(bool $followedByMe): static
    {
        $this->followedByMe = $followedByMe;

        return $this;
    }

    public function getLocales(): array
    {
        return $this->getTranslations()
                    ->map(fn($translation) => $translation->getLocale())
                    ->toArray()
            ;
    }

    public function getTranslation(string $locale): ?SeminarTranslation
    {
        $localeTranslation = $this->getTranslations()->filter(fn($translation) => $translation->getLocale() === $locale)->first();
        if ($localeTranslation) {
            return $localeTranslation;
        }
        return null;
    }

    /**
     * @return Collection<int, SeminarTranslation>
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(SeminarTranslation $translation): static
    {
        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
            $translation->setSeminar($this);
        }

        return $this;
    }

    public function removeTranslation(SeminarTranslation $translation): static
    {
        if ($this->translations->removeElement($translation)) {
            // set the owning side to null (unless already changed)
            if ($translation->getSeminar() === $this) {
                $translation->setSeminar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Presentation>
     */
    public function getPresentations(): Collection
    {
        return $this->presentations;
    }

    public function addPresentation(Presentation $presentation): static
    {
        if (!$this->presentations->contains($presentation)) {
            $this->presentations->add($presentation);
            $presentation->setSeminar($this);
        }

        return $this;
    }

    public function removePresentation(Presentation $presentation): static
    {
        if ($this->presentations->removeElement($presentation)) {
            // set the owning side to null (unless already changed)
            if ($presentation->getSeminar() === $this) {
                $presentation->setSeminar(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
