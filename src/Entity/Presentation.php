<?php

namespace App\Entity;

use App\Repository\PresentationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PresentationRepository::class)]
class Presentation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'presentations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Seminar $seminar = null;

    #[ORM\JoinTable(name: 'seminar_speaker')]
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: 'presentations')]
    private Collection $speakers;

    #[ORM\JoinTable(name: 'seminar_respondant')]
    #[ORM\ManyToMany(targetEntity: Person::class, inversedBy: 'presentationResponses')]
    private Collection $respondants;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $originalTitle = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $beginning = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $ending = null;

    #[ORM\Column]
    private ?bool $followedByMe = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $language = null;

    #[ORM\OneToMany(targetEntity: PresentationTranslation::class, mappedBy: 'presentation', orphanRemoval: true)]
    private Collection $translations;

    public function __construct()
    {
        $this->speakers = new ArrayCollection();
        $this->respondants = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeminar(): ?Seminar
    {
        return $this->seminar;
    }

    public function setSeminar(?Seminar $seminar): static
    {
        $this->seminar = $seminar;

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getSpeakers(): Collection
    {
        return $this->speakers;
    }

    public function addSpeaker(Person $speaker): static
    {
        if (!$this->speakers->contains($speaker)) {
            $this->speakers->add($speaker);
        }

        return $this;
    }

    public function removeSpeaker(Person $speaker): static
    {
        $this->speakers->removeElement($speaker);

        return $this;
    }

    /**
     * @return Collection<int, Person>
     */
    public function getRespondants(): Collection
    {
        return $this->respondants;
    }

    public function addRespondant(Person $respondant): static
    {
        if (!$this->respondants->contains($respondant)) {
            $this->respondants->add($respondant);
        }

        return $this;
    }

    public function removeRespondant(Person $respondant): static
    {
        $this->respondants->removeElement($respondant);

        return $this;
    }

    public function getOriginalTitle(): ?string
    {
        return $this->originalTitle;
    }

    public function setOriginalTitle(string $originalTitle): static
    {
        $this->originalTitle = $originalTitle;

        return $this;
    }

    public function getBeginning(): ?\DateTimeInterface
    {
        return $this->beginning;
    }

    public function setBeginning(\DateTimeInterface $beginning): static
    {
        $this->beginning = $beginning;

        return $this;
    }

    public function getEnding(): ?\DateTimeInterface
    {
        return $this->ending;
    }

    public function setEnding(?\DateTimeInterface $ending): static
    {
        $this->ending = $ending;

        return $this;
    }

    public function isFollowedByMe(): ?bool
    {
        return $this->followedByMe;
    }

    public function setFollowedByMe(bool $followedByMe): static
    {
        $this->followedByMe = $followedByMe;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): static
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return array<string>
     */
    public function getLocales(): array
    {
        return $this->getTranslations()
                    ->map(fn($translation) => $translation->getLocale())
                    ->toArray()
            ;
    }

    public function getTranslation(string $locale): ?PresentationTranslation
    {
        $localeTranslation = $this->getTranslations()
                                  ->filter(fn($translation) => $translation->getLocale() === $locale)
                                  ->first();
        if ($localeTranslation) {
            return $localeTranslation;
        }
        return null;
    }

    /**
     * @return Collection<int, PresentationTranslation>
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(PresentationTranslation $translation): static
    {
        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
            $translation->setPresentation($this);
        }

        return $this;
    }

    public function removeTranslation(PresentationTranslation $translation): static
    {
        if ($this->translations->removeElement($translation)) {
            // set the owning side to null (unless already changed)
            if ($translation->getPresentation() === $this) {
                $translation->setPresentation(null);
            }
        }

        return $this;
    }

    public function getAuthorNames(): string
    {
        return \join(' ', $this->getSpeakers()->map(fn($speaker) => (string) $speaker)->toArray());
    }

    public function __toString(): string
    {
        return $this->getAuthorNames() . ', ' . $this->getOriginalTitle();
    }
}
