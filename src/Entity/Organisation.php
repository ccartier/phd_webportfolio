<?php

namespace App\Entity;

use App\Repository\OrganisationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: OrganisationRepository::class)]
class Organisation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Assert\NotBlank]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $acronym = null;

    #[Assert\Url]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $homepage = null;

    #[ORM\OneToMany(targetEntity: OrganisationTranslation::class, mappedBy: 'organisation', orphanRemoval: true)]
    private Collection $translations;

    #[ORM\ManyToMany(targetEntity: Seminar::class, mappedBy: 'organisation')]
    private Collection $seminars;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->seminars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getAcronym(): ?string
    {
        return $this->acronym;
    }

    public function setAcronym(?string $acronym): static
    {
        $this->acronym = $acronym;

        return $this;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function setHomepage(?string $homepage): static
    {
        $this->homepage = $homepage;

        return $this;
    }

    public function getTranslation(string $locale): ?OrganisationTranslation
    {
        $localeTranslation = $this->getTranslations()->filter(fn($translation) => $translation->getLocale() === $locale)->first();
        if ($localeTranslation) {
            return $localeTranslation;
        }
        return null;
    }

    /**
     * @return Collection<int, OrganisationTranslation>
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(OrganisationTranslation $translation): static
    {
        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
            $translation->setOrganisation($this);
        }

        return $this;
    }

    public function removeTranslation(OrganisationTranslation $translation): static
    {
        if ($this->translations->removeElement($translation)) {
            // set the owning side to null (unless already changed)
            if ($translation->getOrganisation() === $this) {
                $translation->setOrganisation(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, Seminar>
     */
    public function getSeminars(): Collection
    {
        return $this->seminars;
    }

    public function addSeminar(Seminar $seminar): static
    {
        if (!$this->seminars->contains($seminar)) {
            $this->seminars->add($seminar);
            $seminar->addOrganisation($this);
        }

        return $this;
    }

    public function removeSeminar(Seminar $seminar): static
    {
        if ($this->seminars->removeElement($seminar)) {
            $seminar->removeOrganisation($this);
        }

        return $this;
    }
}
