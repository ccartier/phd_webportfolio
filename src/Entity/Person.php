<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PersonRepository::class)]
class Person
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $givenName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $familyName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[Assert\Url]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $homepage = null;

    #[ORM\ManyToMany(targetEntity: Seminar::class, mappedBy: 'organisers')]
    private Collection $organisedSeminars;

    #[ORM\ManyToMany(targetEntity: Presentation::class, mappedBy: 'speakers')]
    private Collection $presentations;

    #[ORM\ManyToMany(targetEntity: Presentation::class, mappedBy: 'respondants')]
    private Collection $presentationResponses;

    public function __construct()
    {
        $this->organisedSeminars = new ArrayCollection();
        $this->presentations = new ArrayCollection();
        $this->presentationResponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(?string $givenName): static
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getFamilyName(): ?string
    {
        return $this->familyName;
    }

    public function setFamilyName(string $familyName): static
    {
        $this->familyName = $familyName;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function setHomepage(?string $homepage): static
    {
        $this->homepage = $homepage;

        return $this;
    }

    #[Assert\NotBlank]
    public function getFullName(): string
    {
        if ($this->getGivenName() || $this->getFamilyName()) {
            $fullName = \join(" ", [$this->getGivenName(), $this->getFamilyName()]);
            if ($this->getName()) {
                return $fullName . ' (' . $this->getName() . ')';
            }
            return $fullName;
        }
        return $this->getName() ? \trim($this->getName()) : "";
    }

    public function __toString(): string
    {
        return $this->getFullName();
    }

    /**
     * @return Collection<int, Seminar>
     */
    public function getOrganisedSeminars(): Collection
    {
        return $this->organisedSeminars;
    }

    public function addOrganisedSeminar(Seminar $organisedSeminar): static
    {
        if (!$this->organisedSeminars->contains($organisedSeminar)) {
            $this->organisedSeminars->add($organisedSeminar);
            $organisedSeminar->addOrganiser($this);
        }

        return $this;
    }

    public function removeOrganisedSeminar(Seminar $organisedSeminar): static
    {
        if ($this->organisedSeminars->removeElement($organisedSeminar)) {
            $organisedSeminar->removeOrganiser($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Presentation>
     */
    public function getPresentations(): Collection
    {
        return $this->presentations;
    }

    public function addPresentation(Presentation $presentation): static
    {
        if (!$this->presentations->contains($presentation)) {
            $this->presentations->add($presentation);
            $presentation->addSpeaker($this);
        }

        return $this;
    }

    public function removePresentation(Presentation $presentation): static
    {
        if ($this->presentations->removeElement($presentation)) {
            $presentation->removeSpeaker($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Presentation>
     */
    public function getPresentationResponses(): Collection
    {
        return $this->presentationResponses;
    }

    public function addPresentationResponse(Presentation $presentationResponse): static
    {
        if (!$this->presentationResponses->contains($presentationResponse)) {
            $this->presentationResponses->add($presentationResponse);
            $presentationResponse->addRespondant($this);
        }

        return $this;
    }

    public function removePresentationResponse(Presentation $presentationResponse): static
    {
        if ($this->presentationResponses->removeElement($presentationResponse)) {
            $presentationResponse->removeRespondant($this);
        }

        return $this;
    }
}
