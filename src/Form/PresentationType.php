<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\Presentation;
use App\Entity\Seminar;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PresentationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('originalTitle')
            ->add('beginning')
            ->add('ending')
            ->add('followedByMe')
            ->add('language')
            ->add('seminar', EntityType::class, [
                'class' => Seminar::class,
            ])
            ->add('speakers', EntityType::class, [
                'class' => Person::class,
                'multiple' => true,
                'required' => false,
            ])
            ->add('respondants', EntityType::class, [
                'class' => Person::class,
                'multiple' => true,
                'required' => false,
            ])
            ->add('translations', CollectionType::class, [
                'entry_type' => PresentationTranslationType::class,
                'allow_add' => true,
                'entry_options' => ['label' => false],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Presentation::class,
        ]);
    }
}
