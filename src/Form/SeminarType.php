<?php

namespace App\Form;

use App\Entity\Organisation;
use App\Entity\Person;
use App\Entity\Seminar;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeminarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('acronym')
            ->add('homepage')
            ->add('organisation', EntityType::class, [
                'class' => Organisation::class,
                'multiple' => true,
                'required' => false,
            ])
            ->add('organisers', EntityType::class, [
                'class' => Person::class,
                'multiple' => true,
                'required' => false,
            ])
            ->add('translations', CollectionType::class, [
                'entry_type' => SeminarTranslationType::class,
                'allow_add' => true,
                'entry_options' => ['label' => false],
            ])
            ->add('followedByMe')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Seminar::class,
        ]);
    }
}
