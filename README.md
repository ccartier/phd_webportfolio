
# Table des matières

1.  [Presentation](#org6cfddc6)
2.  [Data structures](#orgf8523a8)
1.  [Seminars](#org355943a)



<a id="org6cfddc6"></a>

# Presentation

This project serves to present my personnal portfolio, as part of my PhD training.


<a id="orgf8523a8"></a>

# Data structures


<a id="org355943a"></a>

## Seminars

This application should enable me to display the list of seminars

-   that I followed as part of my training, with a small summary of my personnal takeouts
-   that I presented during my PhD, with links to additional resources I might be able to present

In ths future, I also want to display seminars that I may notice during my information watch, and that might interrest other people.

