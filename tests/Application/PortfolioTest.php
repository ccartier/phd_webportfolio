<?php

namespace App\Tests\Application;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PortfolioTestPhpTest extends WebTestCase
{
    public function testHomePage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Bienvenue sur mon site !');
    }

    public function testHomePageEnglish(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Welcome on my personal website!');
    }

    public function testPortfolioIndexPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio');
        $this->assertResponseIsSuccessful();
        $this->assertAnySelectorTextContains('h2', 'Ma formation');
        $this->assertAnySelectorTextContains('h2', 'Mes activités de recherche');
        $this->assertAnySelectorTextContains('h2', 'Enseignements et médiations scientifiques');
        $this->assertAnySelectorTextContains('h2', 'Mes responsabilités collectives');
        // $this->assertAnySelectorTextContains('h2', 'Autres activités');
    }

    public function testPortfolioIndexPageEnglish(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/portfolio');
        $this->assertResponseIsSuccessful();
        $this->assertAnySelectorTextContains('h2', 'My training');
        $this->assertAnySelectorTextContains('h2', 'Research activities');
        $this->assertAnySelectorTextContains('h2', 'Teachings and Scientific popularisation');
        $this->assertAnySelectorTextContains('h2', 'Collective responsabilities');
        // $this->assertAnySelectorTextContains('h2', 'Miscellaneous activities');
    }

    public function testPortfolioTrainingPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/formation');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Ma formation');
        $this->assertAnySelectorTextContains('h2', 'Mon parcours universitaire');
        $this->assertAnySelectorTextContains('h2', 'Les séminaires auxquels j\'assiste');
        $this->markTestIncomplete("Should test if every training item is displayed");
    }

    public function testPortfolioResearchPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/recherche');
        $this->assertResponseIsSuccessful();
        $this->markTestIncomplete("Should test if every talk and organised events are displayed");
    }

    public function testPortfolioPopularisationPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/mediation-scientifique');
        $this->assertResponseIsSuccessful();
        $this->markTestIncomplete("This page is not complete yet");
    }

    public function testPortfolioResponsabilitiesPage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/responsabilites-collectives');
        $this->assertResponseIsSuccessful();
        $this->markTestIncomplete("This page is not complete yet");
    }

    public function testPortfolioMiscelaneousPage(): void
    {
        $this->markTestIncomplete("This page is not yet created");
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/activites-diverses');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Autres activités');
    }
}
