<?php

namespace App\Tests\Unit;

use App\Entity\Organisation;
use App\Entity\OrganisationTranslation;
use App\Entity\Person;
use App\Entity\Seminar;
use App\Entity\SeminarTranslation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class SeminarTest extends KernelTestCase
{
    public function testSeminarEmptyValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $seminar = new Seminar();
        $this->assertCount(1, $validator->validate($seminar), "There should be an error for an organisation with an empty name.");

        $seminar->setName("Test");
        $this->assertCount(0, $validator->validate($seminar), "The error should be resolved by setting the name.");
    }

    public function testSeminarInvalidUrlValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $seminar = new Organisation();
        $seminar->setName("test");
        $seminar->setHomepage("test");
        $this->assertCount(1, $validator->validate($seminar), "There should be an error for an organisation with an invalid homepage.");
        $seminar->setHomepage("https://test.com");
        $this->assertCount(0, $validator->validate($seminar), "The issue should be resolved by having a good URL.");
    }

    public function testSeminarLocaleValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $seminar = new Seminar();
        $seminar->setName("test");

        $translation = new SeminarTranslation();
        $seminar->addTranslation($translation);
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if no locale is set to the translation.");
        $translation->setLocale("random_stuff");
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if the locale is not valid.");
        $translation->setLocale("fr");
        $this->assertCount(0, $validator->validate($translation), "The issue should be solved with a valid locale.");
    }

    public function testGetTranslationForLocale(): void
    {
        $seminar = new Seminar();
        $seminar->setName("test");

        $frenchTranslation = new SeminarTranslation();
        $frenchTranslation->setLocale("fr")->setDescription("Ceci est une description en français");
        $englishTranslation = new SeminarTranslation();
        $englishTranslation->setLocale("en")->setDescription("This is an English description");

        $seminar->addTranslation($frenchTranslation)->addTranslation($englishTranslation);

        $this->assertEquals($frenchTranslation, $seminar->getTranslation("fr"));
        $this->assertEquals($englishTranslation, $seminar->getTranslation("en"));
        $this->assertNull($seminar->getTranslation("de"));
    }
}
