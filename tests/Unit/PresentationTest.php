<?php

namespace App\Tests\Unit;

use App\Entity\Organisation;
use App\Entity\OrganisationTranslation;
use App\Entity\Person;
use App\Entity\Presentation;
use App\Entity\PresentationTranslation;
use App\Entity\Seminar;
use App\Entity\SeminarTranslation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class PresentationTest extends KernelTestCase
{
    public function testPresentationEmptyValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $presentation = new Presentation();
        $this->assertCount(2, $validator->validate($presentation), "There should be an error for a presentation with an empty original title and language.");

        $presentation->setOriginalTitle("Test");
        $this->assertCount(1, $validator->validate($presentation), "One error should be resolved by setting the original title.");

        $presentation->setLanguage("fr");
        $this->assertCount(0, $validator->validate($presentation), "The other error should be resolved by setting the original title.");
    }

    public function testPresentationLocaleValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $presentation = new Presentation();
        $presentation->setOriginalTitle("test");

        $translation = new PresentationTranslation();
        $presentation->addTranslation($translation);
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if no locale is set to the translation.");
        $translation->setLocale("random_stuff");
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if the locale is not valid.");
        $translation->setLocale("fr");
        $this->assertCount(0, $validator->validate($translation), "The issue should be solved with a valid locale.");
    }

    public function testGetTranslationForLocale(): void
    {
        $presentation = new Presentation();
        $presentation->setOriginalTitle("test");

        $frenchTranslation = new PresentationTranslation();
        $frenchTranslation->setLocale("fr")
                          ->setAbstract("Ceci est une description en français")
            ;
        $englishTranslation = new PresentationTranslation();
        $englishTranslation->setLocale("en")
                           ->setAbstract("This is an English description")
            ;

        $presentation->addTranslation($frenchTranslation)
                     ->addTranslation($englishTranslation)
            ;

        $this->assertEquals($frenchTranslation, $presentation->getTranslation("fr"));
        $this->assertEquals($englishTranslation, $presentation->getTranslation("en"));
        $this->assertNull($presentation->getTranslation("de"));
    }

    public function testGetLocaleList(): void
    {
        $presentation = new Presentation();
        $presentation->setOriginalTitle("test");

        $frenchTranslation = new PresentationTranslation();
        $frenchTranslation->setLocale("fr");
        $englishTranslation = new PresentationTranslation();
        $englishTranslation->setLocale("en");

        $presentation->addTranslation($frenchTranslation)
                     ->addTranslation($englishTranslation)
            ;

        $this->assertCount(2, $presentation->getLocales());
        $this->assertEquals(['fr', 'en'], $presentation->getLocales());
    }
}
