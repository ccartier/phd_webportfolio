<?php

namespace App\Tests\Unit;

use App\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class PersonTest extends KernelTestCase
{

    public function testPersonEmptyValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $person = new Person();
        $errors = $validator->validate($person);
        $this->assertCount(1, $errors, "An error should have been raised for a person without name, given name nor family name");

        $person->setName("Test");
        $errors = $validator->validate($person);
        $this->assertCount(0, $errors, "The error should have ben resolved by setting a name");

        $person->setName("");
        $errors = $validator->validate($person);
        $this->assertCount(1, $errors, "An error should have been raised for a person without name, given name nor family name");

        $person->setGivenName("Test");
        $errors = $validator->validate($person);
        $this->assertCount(0, $errors, "The error should have ben resolved by setting a given name");
    }

    public function testPersonHomepageValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $person = new Person();
        $person->setName("John");
        $person->setHomepage("test");
        $errors = $validator->validate($person);
        $this->assertCount(1, $errors);
    }

    public function testGetFullName(): void
    {
        $person = new Person();
        $person->setGivenName("John");
        $person->setFamilyName("Doe");
        $person->setName("Anonymous");
        $this->assertSame("John Doe (Anonymous)", (string) $person);
    }
}
