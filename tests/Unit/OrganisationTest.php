<?php

namespace App\Tests\Unit;

use App\Entity\Organisation;
use App\Entity\OrganisationTranslation;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validation;

class OrganisationTest extends KernelTestCase
{
    public function testOrganisationEmptyValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $organisation = new Organisation();
        $this->assertCount(1, $validator->validate($organisation), "There should be an error for an organisation with an empty name.");

        $organisation->setName("Test");
        $this->assertCount(0, $validator->validate($organisation), "The error should be resolved by setting the name.");
    }

    public function testOrganisationInvalidUrlValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $organisation = new Organisation();
        $organisation->setName("test");
        $organisation->setHomepage("test");
        $this->assertCount(1, $validator->validate($organisation), "There should be an error for an organisation with an invalid homepage.");
        $organisation->setHomepage("https://test.com");
        $this->assertCount(0, $validator->validate($organisation), "The issue should be resolved by having a good URL.");
    }

    public function testOrganisationLocaleValidation(): void
    {
        $validator = Validation::createValidatorBuilder()->enableAttributeMapping()->getValidator();

        $organisation = new Organisation();
        $organisation->setName("test");

        $translation = new OrganisationTranslation();
        $organisation->addTranslation($translation);
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if no locale is set to the translation.");
        $translation->setLocale("random_stuff");
        $this->assertCount(1, $validator->validate($translation), "There should be an issue if the locale is not valid.");
        $translation->setLocale("fr");
        $this->assertCount(0, $validator->validate($translation), "The issue should be solved with a valid locale.");
    }

    public function testGetTranslationForLocale(): void
    {
        $organisation = new Organisation();
        $organisation->setName("test");

        $frenchTranslation = new OrganisationTranslation();
        $frenchTranslation->setLocale("fr")->setDescription("Ceci est une description en français");
        $englishTranslation = new OrganisationTranslation();
        $englishTranslation->setLocale("en")->setDescription("This is an English description");

        $organisation->addTranslation($frenchTranslation)->addTranslation($englishTranslation);

        $this->assertEquals($frenchTranslation, $organisation->getTranslation("fr"));
        $this->assertEquals($englishTranslation, $organisation->getTranslation("en"));
        $this->assertNull($organisation->getTranslation("de"));
    }
}
